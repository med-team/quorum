Source: quorum
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libjellyfish-2.0-dev,
               jellyfish,
               yaggo,
               pkg-config
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/quorum
Vcs-Git: https://salsa.debian.org/med-team/quorum.git
Homepage: https://github.com/gmarcais/Quorum
Rules-Requires-Root: no

Package: quorum
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: QUality Optimized Reads of genomic sequences
 QuorUM enables to obtain trimmed and error-corrected reads that result
 in assemblies with longer contigs and fewer errors. QuorUM provides best
 performance compared to other published error correctors in several
 metrics. QuorUM is efficiently implemented making use of current multi-
 core computing architectures and it is suitable for large data sets (1
 billion bases checked and corrected per day per core). The third-party
 assembler (SOAPdenovo) benefits significantly from using QuorUM error-
 corrected reads. QuorUM error corrected reads result in a factor of 1.1
 to 4 improvement in N50 contig size compared to using the original reads
 with SOAPdenovo for the data sets investigated.
